/* Transaction Processor */
const { TextDecoder } = require("text-encoding/lib/encoding");
const { TransactionHandler } = require("sawtooth-sdk/processor/handler");
const {
  hash,
  writeToStore,
  getProductAddress
} = require("./lib/transactionTp");
const { TransactionProcessor } = require("sawtooth-sdk/processor");
const {
  InvalidTransaction,
  InternalError
} = require("sawtooth-sdk/processor/exceptions");

const FAMILY_NAME = "fmds";
const NAMESPACE = hash(FAMILY_NAME).substring(0, 6);
const URL = "tcp://validator:4004";
var decoder = new TextDecoder("utf8");

function MaterialAdd(
  context,
  supplier,
  packageid,
  materialid,
  materialtype,
  materialweight,
  packingdate,
  originplace
) {
  let product_Address = getProductAddress(packageid, materialid, materialtype);
  let product_detail = [
    supplier,
    packageid,
    materialid,
    materialtype,
    materialweight,
    packingdate,
    originplace
  ];
  context.addEvent("Fmds/MaterialAdd", [
    ["data", JSON.stringify(product_detail)]
  ]);
  var Status = "New Product Added";
  context.addReceiptData(Buffer.from(Status, "utf8"));
  return writeToStore(context, product_Address, product_detail);
}

function TailorAdd(
  context,
  tailorid,
  packageid,
  productid,
  product,
  materialid,
  materialtype,
  productweight,
  arrivaldate,
  dispatchdate
) {
  let address = getProductAddress(packageid, materialid, materialtype);
  return context.getState([address]).then(function(data) {
    console.log("data", data[address]);
    if (data[address] == null || data[address] == "" || data[address] == []) {
      throw new InvalidTransaction("Invalid Product");
    } else {
      let stateJSON = decoder.decode(data[address]);
      let newData =
        stateJSON +
        "," +
        [
          tailorid,
          productid,
          product,
          productweight,
          arrivaldate,
          dispatchdate
        ].join(",");
      let Status = JSON.stringify(newData);
      context.addReceiptData(Buffer.from(Status, "utf8"));
      return writeToStore(context, address, newData);
    }
  });
}
function WholesalerAdd(
  context,
  wholesalerid,
  packageid,
  productid,
  product,
  materialid,
  materialtype,
  productweight,
  arrivaldate,
  dispatchdate,
  retailerid
) {
  let address = getProductAddress(packageid, materialid, materialtype);
  return context.getState([address]).then(function(data) {
    console.log("data", data[address]);
    if (data[address] == null || data[address] == "" || data[address] == []) {
      throw new InvalidTransaction("Invalid Product");
    } else {
      let stateJSON = decoder.decode(data[address]);
      let newData =
        stateJSON +
        "," +
        [
          wholesalerid,
          productid,
          product,
          productweight,
          arrivaldate,
          dispatchdate,
          retailerid
        ].join(",");
      return writeToStore(context, address, newData);
    }
  });
}
function deletePrt(context, packageid, materialid, materialtype) {
  console.log("Deleting Product");
  let address = getProductAddress(packageid, materialid, materialtype);
  console.log("address", address);
  return context.deleteState([address]);
}

//transaction handler class

class Product extends TransactionHandler {
  constructor() {
    super(FAMILY_NAME, ["1.0"], [NAMESPACE]);
  }

  //apply function
  apply(transactionProcessRequest, context) {
    let PayloadBytes = decoder.decode(transactionProcessRequest.payload);
    let Payload = PayloadBytes.toString().split(",");
    let action = Payload[0];

    if (action === "MaterialAdd") {
      return MaterialAdd(
        context,
        Payload[1],
        Payload[2],
        Payload[3],
        Payload[4],
        Payload[5],
        Payload[6],
        Payload[7]
      );
    } else if (action === "TailorAdd") {
      return TailorAdd(
        context,
        Payload[1],
        Payload[2],
        Payload[3],
        Payload[4],
        Payload[5],
        Payload[6],
        Payload[7],
        Payload[8],
        Payload[9]
      );
    } else if (action === "WholesalerAdd") {
      return WholesalerAdd(
        context,
        Payload[1],
        Payload[2],
        Payload[3],
        Payload[4],
        Payload[5],
        Payload[6],
        Payload[7],
        Payload[8],
        Payload[9],
        Payload[10]
      );
    } else if (action === "DeleteProduct") {
      return deletePrt(context, Payload[1], Payload[2], Payload[3]);
    }
    {
      console.log("error");
    }
  }
}
const transactionProcesssor = new TransactionProcessor(URL);
transactionProcesssor.addHandler(new Product());
transactionProcesssor.start();
