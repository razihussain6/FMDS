                                                
------------------------------------------------------------------------ PUBLIC DISTRIBUTION SYSTEM  --------------------------------------------------------------------------------------------------------


Note:**Use the Correct Private Key of each Department for transactions because of Implementation of permission** 

**UI  User Guide**
1. Open http://localhost:3000 in the chrome browser.
2. Navigates to the FMDS 'Home page' with "WELCOME" message.
3. Navigate to 'Material Management'page,Fill all the fields in the forms :
   Supplier ID: Agency or suppliers[permission is set only for the private agency]
   Material ID: MAterial Id of fabric raw-material produced by the local suppliers
   Agency PrivateKey: The key you obtained by key generation[ # sawthooth keygen Agency]
   Package id : pid should be unique for each fabric material type
   Choose Product Type:Select it from drop down
   choose Net weight: select it from drop down
   Date of Packing : Packing date
   Place of Origin : Select the State from where the product is cultivated
   #Add the product[submit the form]
   
4. Navigate to 'Tailor'page,Fill all the fields in the forms :
   Supplier ID: Tailor [permission is set only for Tailor]
   Tailor ID: Tailor [permission is set only for Tailor]
   Tailor PrivateKey: The key you obtained by key generation[ # sawthooth keygen CentralDept]
   Package id : pid should be unique for each fabric material type
   Product Type: Select it from drop down
   Material ID: MAterial Id of fabric raw-material produced by the local suppliers
   Material Type: Select it from drop down
   Material Net Weight: select it from drop down
   Package Arrival date: select the arrival date
   Package Dispatch date: Select the dispatch date
   State to be sent : Select the state where the product is alocated to [choose either Kerala.Since we've only set permission for this states only at present] 
   #Add the product[submit the form]

5. Navigate to 'Wholesaler'page,Fill all the fields in the forms :
   Package id : pid should be unique for each fabric material type
   Wholesaler PrivateKey: The key you obtained by key generation[ # sawthooth keygen CentralDept]
   Wholesaler ID: Wholesaler [permission is set only for Wholesaler]
   Product id : Prid should be unique for each fabric material type
   Product Type:Select it from drop down
   Material ID: MAterial Id of fabric raw-material produced by the local suppliers
   Material Type: Select it from drop down
   Product Net weight: select it from drop down
   Package Arrival date: select the arrival date
   Package Dispatch date: Select the dispatch date
   Choose Retailer : Select the Retailer outlet where the product is alocated to  
   #Add the product[submit the form]

6. Navigate to 'RETAILER'page,Fill all the fields in the forms :
   Retailer StoreID:choose required retailer outlet id.
   press view stock button to view Package ID ,Product and Weight.
#Here you have 2 buttons : View Details and Delete Product
To know more, Enter
Retailer StoreID:Retailer: [permission is set only for Retailer]
Retailer Private Key: The key you obtained by key generation[ # sawthooth keygen CentralDept]
Package ID: pid should be unique for each fabric material type
Net Weight: select it from drop down
Package Dispatch date: Select the dispatch date
Product Type: Select the dispatch date
6.1 Click 'View Details' Button :You can view the below mentioned details of the product
    Place of Orgin
    Date of Packing
    Package Arrival Date
    Package Dispatch Date 
    Wholesale Arrival Date
    Wholesale Dispatch Date
    Retaile Arrival Date


6.2 Click 'Delete Product' Button:
    If product is delete you will get a alert message "Data Succesfully Deleted" and redirect to Home page
    
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
**Client side Transaction Flow**

1. Material Management ---- When the user enters the data in the "Material Management" page the function "MaterialAdd()" in core.js file is triggered and the entered data is posted to the "/addMaterial" in index.js file, which triggers the "MaterialAdd()" function in UserClient.js file in which the data entered is set into a payload and the payload, inputaddresslist, outputaddresslist, familyname, familyversion, etc are passed as parameter to trigger "createProduct" function and the transactions is made into batches and batches are finally send to the Transaction Processors via rest-api port 8008/batches. 

2. Tailor ---- When the user enters a data in the "Tailor" page the function "Central()" in core.js file is triggered and the entered data is posted to the "/TailorAdd" in index.js file, which triggers the "TailorAdd()" Function in UserClient.js file in which the data entered is set into a payload and the payload, inputaddresslist, Outputaddresslist, familyname, familyversion, etc are passed as parameter to trigger "TailorAdd()" function and the transactions is made into batches and batches are finally send to the Transaction Processors via rest-api port 8008/batches. 

3. Wholesaler  ---- When the user enters a data in the "Wholesaler" page the function "WholesalerAdd()" in core.js file is triggered and the entered data is posted to the "/WholesalerAdd" in index.js file, which triggers the "WholesalerAdd()" Function in UserClient.js file in which the data entered is set into a payload and the payload, inputaddresslist, Outputaddresslist, familyname, familyversion, etc are passed as parameter to trigger "createProduct()" function and the transactions is made into batches and batches are finally send to the Transaction Processors via rest-api port 8008/batches.

4. Retailer ---- When the user enters a data in the "RETAILER" page have 2 functions "Retrieve()" and "Delete()"."Retrieve()" in main.js file is triggered and the entered data is get to the "/state/:data1/:data2/:data3" in index.js file, which triggers the "RetailerAdd()" Function in UserClient.js file and query the address .then come back to main.js and list it.
"Delete()" in main.js file is triggered and the entered data is posted to the "/delete" in index.js file, which triggers the "DeletePrt()" Function in UserClient.js file in which the data entered is set into a payload and the payload, inputaddresslist, Outputaddresslist, familyname, familyversion, etc are passed as parameter to trigger "RetailerAdd()" function and the transactions is made into batches and batches are finally send to the Transaction Processors via rest-api port 8008/batches.



 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

**Transaction Processor side Transaction Flow**

1. Material Management  ---- When the transaction from the client side through rest-api arrives at the Transaction Family "FMDS", and the action is 'Material Add' then it triggers the function "MAterialAdd()" in the Tp.js file. The address to which the data is to be written is generated using the sackid,product,weight in the payload . After that,triggers the function "writeToStore()" which will produce an event if the payload consist the parameter needed to create the address. The data is written to the state by function 'setState'.



2. Tailor ---- When the transaction from the client side through rest-api arrives at the Transaction Family "FMDS", and the action is 'Central Add' then it triggers the function "addCentral()" in the Tp.js file. The address to which the data is to be written is generated using the sackid,product,weight in the payload . After that, the current state of the generated address is queried and if the state data is empty or null ,returns an InvalidTransaction stating that "Invalid Sack id or Product or weight".else if there is state data the new payload is appended to the existing state data 

3. Wholesaler ---- When the transaction from the client side through rest-api arrives at the Transaction Family "FMDS", and the action is 'Wholesaler Add' then it triggers the function "WholesalerAdd()" in the Tp.js file. The address to which the data is to be written is generated using the sackid,product,weight in the payload . After that, the current state of the generated address is queried and if the state data is empty or null ,returns an InvalidTransaction stating that "Invalid Sack id or Product or weight".else if there is state data the new payload is appended to the existing state data 

4. Retailer  ---- When the transaction from the client side through rest-api arrives at the Transaction Family "FMDS", and the action is 'Delete Product' then it triggers the function "deletePrt()" in the Tp.js file. The address to which the data is to be written is generated using the packageid,product,weight in the payload . Then triggers the function "deleteState()" which will delete the data from the addres

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------







