function MaterialAdd(event) {
  event.preventDefault();
  let supplier = document.getElementsByName("Sid")[0].value;
  let packageid = document.getElementsByName("pid")[0].value;
  let privatekey = document.getElementsByName("AgencyPrivateKey")[0].value;
  let materialid = document.getElementsByName("Mid")[0].value;
  let materialtype = document.getElementsByName("materialtype")[0].value;
  let materialweight = document.getElementsByName("materialweight")[0].value;
  let packingdate = document.getElementsByName("dateofpacking")[0].value;
  let originplace = document.getElementsByName("State")[0].value;
  if (
    supplier != "" &&
    packageid != "" &&
    privatekey != "" &&
    materialid != "" &&
    materialtype != "" &&
    packingdate != "" &&
    materialweight != "" &&
    originplace != ""
  ) {
    jQuery.post(
      "/MaterialAdd",
      {
        supplier: supplier,
        packageid: packageid,
        privatekey: privatekey,
        materialid: materialid,
        materialtype: materialtype,
        packingdate: packingdate,
        materialweight: materialweight,
        originplace: originplace
      },
      function(data) {
        if (data.status != 202) alert("your data status : " + data.text);
      },
      "json"
    );
  } else {
    alert("Fill all the Details");
  }
}
function TailorAdd(event) {
  event.preventDefault();
  let tailorid = document.getElementsByName("Tid")[0].value;
  let packageid = document.getElementsByName("pid")[0].value;
  let privatekey = document.getElementsByName("TailorPrivateKey")[0].value;
  let productid = document.getElementsByName("Prid")[0].value;
  let product = document.getElementsByName("product")[0].value;
  let materialid = document.getElementsByName("Mid")[0].value;
  let materialtype = document.getElementsByName("materialtype")[0].value;
  let productweight = document.getElementsByName("productweight")[0].value;
  let arrivaldate = document.getElementsByName("arrivaldate")[0].value;
  let dispatchdate = document.getElementsByName("dispatchdate")[0].value;
  if (
    tailorid != "" &&
    packageid != "" &&
    privatekey != "" &&
    productid != "" &&
    product != "" &&
    materialid != "" &&
    materialtype != "" &&
    productweight != "" &&
    arrivaldate != "" &&
    dispatchdate != ""
  ) {
    jQuery.post(
      "/TailorAdd",
      {
        tailorid: tailorid,
        packageid: packageid,
        privatekey: privatekey,
        productid: productid,
        product: product,
        materialid: materialid,
        materialtype: materialtype,
        productweight: productweight,
        arrivaldate: arrivaldate,
        dispatchdate: dispatchdate
      },
      function(data) {
        if (data.status != 202) alert("your data status : " + data.text);
      },
      "json"
    );
  } else {
    alert("Fill all the Details");
  }
}

function WholesalerAdd(event) {
  event.preventDefault();
  let wholesalerid = document.getElementsByName("Wid")[0].value;
  let packageid = document.getElementsByName("pid")[0].value;
  let privatekey = document.getElementsByName("WholesalePrivateKey")[0].value;
  let productid = document.getElementsByName("Prid")[0].value;
  let product = document.getElementsByName("product")[0].value;
  let materialid = document.getElementsByName("Mid")[0].value;
  let materialtype = document.getElementsByName("materialtype")[0].value;
  let productweight = document.getElementsByName("productweight")[0].value;
  let arrivaldate = document.getElementsByName("arrivaldate")[0].value;
  let dispatchdate = document.getElementsByName("dispatchdate")[0].value;
  let retailerid = document.getElementsByName("retailerid")[0].value;
  if (
    wholesalerid != "" &&
    packageid != "" &&
    privatekey != "" &&
    productid != "" &&
    product != "" &&
    materialid != "" &&
    materialtype != "" &&
    productweight != "" &&
    arrivaldate != "" &&
    dispatchdate != "" &&
    retailerid != ""
  ) {
    jQuery.post(
      "/WholesalerAdd",
      {
        wholesalerid: wholesalerid,
        packageid: packageid,
        privatekey: privatekey,
        productid: productid,
        product: product,
        materialid: materialid,
        materialtype: materialtype,
        productweight: productweight,
        arrivaldate: arrivaldate,
        dispatchdate: dispatchdate,
        retailerid: retailerid
      },
      function(data) {
        if (data.status != 202) alert("your data status : " + data.text);
      },
      "json"
    );
  } else {
    alert("Fill all the Details");
  }
}
function Retrieve(event) {
  event.preventDefault();
  let retailerid = document.getElementsByName("retailer")[0].value;
  let packageid = document.getElementsByName("pid")[0].value;
  let privatekey = document.getElementsByName("RetailerPrivateKey")[0].value;
  let materialid = document.getElementsByName("Mid")[0].value;
  let materialtype = document.getElementsByName("materialtype")[0].value;
  if (
    retailerid != "" &&
    packageid != "" &&
    privatekey != "" &&
    materialid != "" &&
    materialtype != ""
  ) {
    jQuery.post(
      "/Retrieve",
      {
        retailerid: retailerid,
        packageid: packageid,
        privatekey: privatekey,
        materialid: materialid,
        materialtype: materialtype
      },
      function(data) {
        if (data.status != 202) {
          if (data.dataR != "not exist") {
            let rdata = data.dataR;
            document.getElementById("tproduct").value = rdata[9];
            document.getElementById("torigin").value = rdata[6];
            document.getElementById("tpacking").value = rdata[5];
            document.getElementById("Tarrival").value = rdata[11];
            document.getElementById("Tdispatch").value = rdata[12];
            document.getElementById("Warrival").value = rdata[17];
            document.getElementById("Wdispatch").value = rdata[18];
            console.log(rdata);
          } else {
            alert("Product not exist");
          }
        }
      },
      "json"
    );
  } else {
    alert("Fill all the Details");
  }
}
function Delete(event) {
  event.preventDefault();
  let retailerid = document.getElementsByName("retailer")[0].value;
  let packageid = document.getElementsByName("pid")[0].value;
  let privatekey = document.getElementsByName("RetailerPrivateKey")[0].value;
  let materialid = document.getElementsByName("Mid")[0].value;
  let materialtype = document.getElementsByName("materialtype")[0].value;
  if (
    retailerid != "" &&
    packageid != "" &&
    privatekey != "" &&
    materialid != "" &&
    materialtype != ""
  ) {
    jQuery.post(
      "/Delete",
      {
        retailerid: retailerid,
        packageid: packageid,
        privatekey: privatekey,
        materialid: materialid,
        materialtype: materialtype
      },
      function(data) {
        if (data.status != 202) {
          alert("your data status : " + data.text);
        }
      },
      "json"
    );
  } else {
    alert("Fill all the Details");
  }
}
function getProducts(event) {
  event.preventDefault();
  let retailerid = document.getElementsByName("sretailer")[0].value;
  if (retailerid != "") {
    jQuery.post(
      "/getProducts",
      {
        retailerid: retailerid
      },
      function(data) {
        if (data.status != 202) {
          console.log(data.dataR);
          var pdata = JSON.parse(data.dataR);
          var table = document.getElementById("productlist");
          table.innerHTML = "";
          pdata.forEach(element => {
            if (element) {
              var row = table.insertRow(0);
              var cell1 = row.insertCell(0);
              var cell2 = row.insertCell(1);
              var cell3 = row.insertCell(2);
              var cell4 = row.insertCell(3);
              cell1.innerHTML = "";
              cell2.innerHTML = element[8];
              cell3.innerHTML = element[9];
              cell4.innerHTML = element[10];
            }
          });
        }
      },
      "json"
    );
  } else {
    alert("Fill all the Details");
  }
}
