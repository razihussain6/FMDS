var express = require("express");
var { Product } = require("./UserClient");
const { getProductAddress, getAllProducts } = require("./lib/transaction");
var router = express.Router();

/* GET home page. */
router.get("/", function(req, res, next) {
  res.render("index");
});

router.get("/material", function(req, res, next) {
  res.render("material");
});
router.get("/tailor", function(req, res, next) {
  res.render("tailor");
});
router.get("/wholesaler", function(req, res, next) {
  res.render("wholesaler");
});
router.get("/retailer", function(req, res, next) {
  res.render("retailer");
});

router.post("/MaterialAdd", async function(req, res) {
  let supplier = req.body.supplier;
  let packageid = req.body.packageid;
  let privatekey = req.body.privatekey;
  let materialid = req.body.materialid;
  let materialtype = req.body.materialtype;
  let materialweight = req.body.materialweight;
  let packingdate = req.body.packingdate;
  let originplace = req.body.originplace;

  console.log("Data sent to REST API");
  var client = new Product();
  await client.MaterialAdd(
    supplier,
    packageid,
    privatekey,
    materialid,
    materialtype,
    materialweight,
    packingdate,
    originplace
  );
  res.send({ text: "Data successfully added" });
});
router.post("/TailorAdd", async function(req, res) {
  let tailorid = req.body.tailorid;
  let packageid = req.body.packageid;
  let privatekey = req.body.privatekey;
  let productid = req.body.productid;
  let product = req.body.product;
  let materialid = req.body.materialid;
  let materialtype = req.body.materialtype;
  let productweight = req.body.productweight;
  let arrivaldate = req.body.arrivaldate;
  let dispatchdate = req.body.dispatchdate;

  console.log("Data sent to REST API");
  var client = new Product();
  await client.TailorAdd(
    tailorid,
    packageid,
    privatekey,
    productid,
    product,
    materialid,
    materialtype,
    productweight,
    arrivaldate,
    dispatchdate
  );
  res.send({ text: "Data successfully added" });
});
router.post("/WholesalerAdd", async function(req, res) {
  let wholesalerid = req.body.wholesalerid;
  let packageid = req.body.packageid;
  let privatekey = req.body.privatekey;
  let productid = req.body.productid;
  let product = req.body.product;
  let materialid = req.body.materialid;
  let materialtype = req.body.materialtype;
  let productweight = req.body.productweight;
  let arrivaldate = req.body.arrivaldate;
  let dispatchdate = req.body.dispatchdate;
  let retailerid = req.body.retailerid;

  console.log("Data sent to REST API");
  var client = new Product();
  await client.WholesalerAdd(
    wholesalerid,
    packageid,
    privatekey,
    productid,
    product,
    materialid,
    materialtype,
    productweight,
    arrivaldate,
    dispatchdate,
    retailerid
  );
  res.send({ text: "Data successfully added" });
});

router.post("/Retrieve", async function(req, res) {
  try {
    let retailerid = req.body.retailerid;
    let packageid = req.body.packageid;
    let privatekey = req.body.privatekey;
    let materialid = req.body.materialid;
    let materialtype = req.body.materialtype;

    let address = await getProductAddress(packageid, materialid, materialtype);
    var client = new Product();

    let stable = await client.getState(address, true);
    let Data = stable.data[0].data;
    let newData = Buffer.from(Data, "base64").toString();
    let DataStored = newData.split(",");
    console.log("DataStored", DataStored);
    res.send({ dataR: DataStored });
  } catch (err) {
    res.send({ dataR: "not exist" });
  }
});

router.post("/Delete", async function(req, res) {
  let retailerid = req.body.retailerid;
  let packageid = req.body.packageid;
  let privatekey = req.body.privatekey;
  let materialid = req.body.materialid;
  let materialtype = req.body.materialtype;
  console.log("Data sent to REST API");
  var client = new Product();
  await client.DeleteProduct(privatekey, packageid, materialid, materialtype);
  res.send({ text: "Data successfully Deleted" });
});

router.post("/getProducts", async function(req, res) {
  try {
    let retailerid = req.body.retailerid;
    console.log("rid", retailerid);
    let address = await getAllProducts();
    var client = new Product();
    let stable = await client.getState(address, true);
    console.log("stable", stable);
    var retailerdata = stable.data.map(y => {
      let x = Buffer.from(y.data, "base64").toString();
      let z = x.split(",");
      let retailer = z.pop();
      if (retailer == retailerid) {
        return z;
      }
    });

    console.log("retailer data:", retailerdata);
    res.send({ dataR: JSON.stringify(retailerdata) });
  } catch (err) {
    res.send({ dataR: "not exist" });
  }
});

module.exports = router;
