const { createProduct } = require("./lib/processor");
const { getProductAddress } = require("./lib/transaction");
const fetch = require("node-fetch");

//family name
FAMILY_NAME = "fmds";

// class for product
class Product {
  MaterialAdd(
    supplier,
    packageid,
    privatekey,
    materialid,
    materialtype,
    materialweight,
    packingdate,
    originplace
  ) {
    let address = getProductAddress(packageid, materialid, materialtype);
    let action = "MaterialAdd";
    console.log(action);
    let payload = [
      action,
      supplier,
      packageid,
      materialid,
      materialtype,
      materialweight,
      packingdate,
      originplace
    ].join(",");
    if (supplier == "Supplier") {
      console.log("payload", payload);
      createProduct(FAMILY_NAME, [address], [address], privatekey, payload);
    } else {
      console.log("UnAuthorised Supplier");
    }
  }

  TailorAdd(
    tailorid,
    packageid,
    privatekey,
    productid,
    product,
    materialid,
    materialtype,
    productweight,
    arrivaldate,
    dispatchdate
  ) {
    let address = getProductAddress(packageid, materialid, materialtype);
    let action = "TailorAdd";
    console.log(action);
    let payload = [
      action,
      tailorid,
      packageid,
      productid,
      product,
      materialid,
      materialtype,
      productweight,
      arrivaldate,
      dispatchdate
    ].join(",");
    if (tailorid == "Tailor") {
      console.log("payload", payload);
      createProduct(FAMILY_NAME, [address], [address], privatekey, payload);
    } else {
      console.log("UnAuthorised Tailor");
    }
  }

  WholesalerAdd(
    wholesalerid,
    packageid,
    privatekey,
    productid,
    product,
    materialid,
    materialtype,
    productweight,
    arrivaldate,
    dispatchdate,
    retailerid
  ) {
    let address = getProductAddress(packageid, materialid, materialtype);
    let action = "WholesalerAdd";
    console.log(action);
    let payload = [
      action,
      wholesalerid,
      packageid,
      productid,
      product,
      materialid,
      materialtype,
      productweight,
      arrivaldate,
      dispatchdate,
      retailerid
    ].join(",");
    if (wholesalerid == "Wholesaler") {
      console.log("payload", payload);
      createProduct(FAMILY_NAME, [address], [address], privatekey, payload);
    } else {
      console.log("UnAuthorised Tailor");
    }
  }

  DeleteProduct(key, packageid, materialid, materialtype) {
    let address = getProductAddress(packageid, materialid, materialtype);
    let action = "DeleteProduct";
    console.log(action);
    let payload = [action, packageid, materialid, materialtype].join(",");
    console.log("payload", payload);
    createProduct(FAMILY_NAME, [address], [address], key, payload);
  }

  /**
   * Get state from the REST API
   * @param {*} address The state address to get
   * @param {*} isQuery Is this an address space query or full address
   */
  async getState(address, isQuery) {
    let stateRequest = "http://rest-api:8008/state";
    if (address) {
      if (isQuery) {
        stateRequest += "?address=";
      } else {
        stateRequest += "/address/";
      }
      stateRequest += address;
    }
    let stateResponse = await fetch(stateRequest);
    let stateJSON = await stateResponse.json();
    return stateJSON;
  }
}

module.exports = {
  Product
};
