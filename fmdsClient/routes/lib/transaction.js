/*
module to deal with address generation and hashing 
*/

const crypto = require("crypto");
const { TextEncoder } = require("text-encoding/lib/encoding");
const { Secp256k1PrivateKey } = require("sawtooth-sdk/signing/secp256k1");
const { createContext, CryptoFactory } = require("sawtooth-sdk/signing");

var encoder = new TextEncoder("utf8");

// function to hash data
function hash(data) {
  return crypto
    .createHash("sha512")
    .update(data)
    .digest("hex");
}

/* function to retrive the address of a particular product  based on its sack id,product type,weight */

function getProductAddress(packageid, material, type) {
  let nameHash = hash("fmds");
  let sHash = hash(packageid);
  let pHash = hash(material);
  let wHash = hash(type);
  return (
    nameHash.slice(0, 6) +
    pHash.slice(0, 26) +
    wHash.slice(0, 24) +
    sHash.slice(0, 14)
  );
}

function getAllProducts() {
  let nameHash = hash("fmds");
  return nameHash.slice(0, 6);
}

module.exports = {
  hash,
  getProductAddress,
  getAllProducts
};
